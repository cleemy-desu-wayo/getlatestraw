#!/bin/sh
#
# ==========================================================================
# getlatestraw.sh -- version 0.2 (2024-01-02)
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/getlatestraw
# ==========================================================================
#
# get latest version from GitLab.com
#
# CAUTION: this is for my personal use
#
# Usage:
#  $ ./getlatestraw.sh outvoke samples/vrchat_join_log.rb
#

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

case "${1}" in
cdw)    base_url='https://gitlab.com/cleemy-desu-wayo/cleemy-desu-wayo/' ;;
[a-z]*) base_url=$(printf '%s\n' "https://gitlab.com/cleemy-desu-wayo/${1}" | head -1)'/' ;;
'' )
  cat <<EOS
getlatestraw.sh -- version 0.2 (2024-01-02)
Usage:
  $ ./getlatestraw.sh outvoke samples/vrchat_join_log.rb
EOS
  exit 0
;;
*)      echo 'ERROR: invalid option'     1>&2 ; exit 1 ;;
esac

case "${2}" in
'' ) file_path='README.md' ;;
*)   file_path=$(printf '%s\n' "${2}" | head -1) ;;
esac

#echo "base_url  : ${base_url}"  # DEBUG
#echo "file_path : ${file_path}" # DEBUG

{
  echo '################################################################################'
  echo '# getlatestraw.sh -- version 0.2 (2024-01-02)'
  
  url=$(printf '%s%s\n' "${base_url}-/raw/main/${file_path}" | head -1 | sed 's/[^-_/.:0-9a-zA-Z]//g')
  echo "# get ${url}" | head -1
  echo '################################################################################'
} 1>&2
#exit

sleep 1

wget "${url}" -O -

exit $?
